package ch.zhaw.catan.model;

import ch.zhaw.catan.Config;

import java.util.Map;

/**
 * This class contains the stock of all the resources for the bank
 *
 * @author Alina, Pascal, Ridvan, Nils
 * @version 01.12.2020
 */
public class Bank extends ResourceOwner {
    // how many resources are required in exchange for a specific resource of the bank
    public static final int REQUIRED_RESOURCES_FOR_TRADE = 4;
    // how many resources are returned by the bank in a trade
    public static final int RETURNED_RESOURCES_FOR_TRADE = 1;

    /**
     * Create a new instance of the {@code Bank} class and sets the initial stock of the resources
     */
    public Bank() {
        super(); // sets the stock of all resources to zero (in case that INITIAL_RESOURCE_CARDS_BANK is incomplete)
        for (Map.Entry<Config.Resource, Integer> entry : Config.INITIAL_RESOURCE_CARDS_BANK.entrySet()) {
            setResourceStock(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Increase the {@code resourceOwner}s stock of the resource and decrease the stock of the bank
     *
     * @param resourceOwner whose stock will be increased
     * @param resource      the resource
     * @param amount        the amount by which the resource is increased
     */
    public void payoutResource(ResourceOwner resourceOwner, Config.Resource resource, int amount) {
        resourceOwner.increaseResourceStock(resource, amount);
        decreaseResourceStock(resource, amount);
    }

    /**
     * Decrease the {@code resourceOwner}s stock of the resource and increase the stock of the bank
     *
     * @param resourceOwner whose stock will be decreased
     * @param resource      the resource
     * @param amount        the amount by which the resource is decreased
     */
    public void depositResource(ResourceOwner resourceOwner, Config.Resource resource, int amount) {
        resourceOwner.decreaseResourceStock(resource, amount);
        increaseResourceStock(resource, amount);
    }
}
