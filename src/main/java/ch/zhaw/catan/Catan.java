package ch.zhaw.catan;

import ch.zhaw.catan.io.Command;
import ch.zhaw.catan.io.Text;
import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import org.beryx.textio.TextTerminal;

import java.awt.*;
import java.util.Random;

/**
 * This class contains the {@code main} method
 * It initializes the {@code SiedlerGame} and handles all the games phases including user inputs during phase 2 and 3
 *
 * @author Pascal, Ridvan, Nils
 * @version 04.12.2020
 */
public class Catan {
    private static final int WIN_POINTS = 7; // because of longest road

    private static final int NUMBER_OF_DICES = 2;
    private static final int DICE_MIN_VALUE = 1;
    private static final int DICE_MAX_VALUE = 6;

    private final TextIO textIO;
    private final TextTerminal<?> textTerminal;

    private final Random random;

    private SiedlerGame siedlerGame;

    /**
     * the {@code main} method creates an instance of {@code Catan} starts the game
     *
     * @param args arguments (none)
     */
    public static void main(String[] args) throws Exception {
        new Catan().start();
    }

    /**
     * Constructor initializes variables
     */
    private Catan() {
        textIO = TextIoFactory.getTextIO();
        textTerminal = textIO.getTextTerminal();
        random = new Random();
    }

    /**
     * run the game (establishment and gameplay phase)
     */
    private void start() throws Exception {
        startPhaseEstablishment();
        startPhaseGameplay();
    }

    /**
     * Starts the second phase where the amount of players is entered and the initial settlements and roads are placed
     */
    private void startPhaseEstablishment() throws Exception {
        int playersCount = readInt(Text.PROMPT_PLAYERS_COUNT,
                Config.MIN_NUMBER_OF_PLAYERS, Config.Faction.values().length);
        if(playersCount < Config.MIN_NUMBER_OF_PLAYERS) {
            throw new Exception("Not enought players");
        }
        siedlerGame = new SiedlerGame(WIN_POINTS, playersCount);

        // each player places the first settlement and road
        for (int i = 0; i < playersCount; i++) {
            printSiedlerBoard();
            placeInitialSettlement(false);
            placeInitialRoad();
            siedlerGame.switchToNextPlayer();
        }

        // each player places the second settlement and road
        for (int i = 0; i < playersCount; i++) {
            printSiedlerBoard();
            siedlerGame.switchToPreviousPlayer();
            placeInitialSettlement(true);
            placeInitialRoad();
        }
    }

    /**
     * Asks the user for the location of an initial settlement and places it
     *
     * @param payout true, if the player shall get the resources of the surrounding fields
     */
    private void placeInitialSettlement(boolean payout) {
        print(Text.INITIAL_SETTLEMENT, siedlerGame.getCurrentPlayerFaction().name());
        while (!siedlerGame.placeInitialSettlement(readPoint(), payout)) {
            print(Text.INVALID_INPUT);
        }
    }

    /**
     * Asks the user for the location of an initial road and places it
     */
    private void placeInitialRoad() {
        print(Text.INITIAL_ROAD, siedlerGame.getCurrentPlayerFaction().name());
        while (!siedlerGame.placeInitialRoad(readPoint(), readPoint())) {
            print(Text.INVALID_INPUT);
        }
    }

    /**
     * initially print the game and throw the dice for the first time and then start reading and executing user commands
     * the commands will be read until a player has won the game, in the end a winner message will be printed
     */
    private void startPhaseGameplay() {
        printSiedlerBoard();
        throwDice();

        Config.Faction winner;
        while ((winner = siedlerGame.getWinner()) == null) {
            switch (readEnumValue(Command.class, Text.PROMPT_COMMAND)) {
                case TRADE:
                    handleTradeCommand();
                    break;
                case BUILD_ROAD:
                    handleBuildRoadCommand();
                    break;
                case BUILD_SETTLEMENT:
                    handleBuildSettlementCommand();
                    break;
                case BUILD_CITY:
                    handleBuildCityCommand();
                    break;
                case SHOW_RESOURCES:
                    handleShowResourcesCommand();
                    break;
                case END_TURN:
                    handleEndTurnCommand();
                    break;
                case QUIT:
                    System.exit(0);
                default:
                    print(Text.INVALID_INPUT);
            }
        }

        print(Text.FACTION_WINNER, winner.name());
    }

    /**
     * handle the trade action with the bank
     */
    private void handleTradeCommand() {
        Config.Resource requested = readEnumValue(Config.Resource.class, Text.PROMPT_TRADE_REQUESTED);
        Config.Resource offer = readEnumValue(Config.Resource.class, Text.PROMPT_TRADE_OFFER);

        if (!siedlerGame.tradeWithBankFourToOne(offer, requested)) {
            print(Text.INVALID_INPUT);
        }
    }

    /**
     * handle build settlement command
     */
    private void handleBuildSettlementCommand() {
        Point settlementLocation = readPoint(Text.PROMPT_SETTLEMENT_LOCATION);

        if (siedlerGame.buildSettlement(settlementLocation)) {
            printSiedlerBoard();
        } else {
            print(Text.INVALID_INPUT);
        }
    }

    /**
     * handle build city command
     */
    private void handleBuildCityCommand() {
        Point cityLocation = readPoint(Text.PROMPT_CITY_LOCATION);

        if (siedlerGame.buildCity(cityLocation)) {
            printSiedlerBoard();
        } else {
            print(Text.INVALID_INPUT);
        }
    }

    /**
     * handle build road command
     */
    private void handleBuildRoadCommand() {
        Point roadStart = readPoint(Text.PROMPT_ROAD_START_POINT);
        Point roadEnd = readPoint(Text.PROMPT_ROAD_END_POINT);

        if (siedlerGame.buildRoad(roadStart, roadEnd)) {
            printSiedlerBoard();
        } else {
            print(Text.INVALID_INPUT);
        }
    }

    /**
     * handle the show resources command
     */
    private void handleShowResourcesCommand() {
        print(Text.SHOW_RESOURCES, siedlerGame.getCurrentPlayerFaction().name());
        for (Config.Resource resource : Config.Resource.values()) {
            print(Text.SHOW_RESOURCES_VALUE, resource.name(), siedlerGame.getCurrentPlayerResourceStock(resource));
        }
    }

    /**
     * print the board to the terminal
     */
    private void printSiedlerBoard() {
        textTerminal.println(siedlerGame.getBoard().getSiedlerBoardTextView().toString());
    }

    /**
     * switch to the next player and throw the dices again
     */
    private void handleEndTurnCommand() {
        siedlerGame.switchToNextPlayer();
        throwDice();
    }

    /**
     * generate the value of the dice throw and call {@link SiedlerGame#throwDice}
     */
    private void throwDice() {
        int diceThrow = 0;
        for (int i = 0; i < NUMBER_OF_DICES; i++) {
            diceThrow += random.nextInt(DICE_MAX_VALUE - DICE_MIN_VALUE) + DICE_MIN_VALUE; // value between MIN and MAX (inclusive)
        }
        print(Text.START_OF_TURN, siedlerGame.getCurrentPlayerFaction().name(), diceThrow);
        siedlerGame.throwDice(diceThrow);
    }

    /**
     * Prompt the user to enter a point (coordinates)
     *
     * @param text description for the coordinate which should be entered
     * @return entered coordinate
     */
    private Point readPoint(Text text) {
        print(text);
        return readPoint();
    }

    /**
     * Prompt the user to enter a point (coordinates)
     *
     * @return entered coordinate
     */
    private Point readPoint() {
        Point point = new Point();
        point.x = readInt(Text.PROMPT_POINT_X_AXIS);
        point.y = readInt(Text.PROMPT_POINT_Y_AXIS);
        return point;
    }

    /**
     * Read an integer within the range of {@code min} and {@code max} from the {@code textIO}
     *
     * @param text prompt message
     * @param min  min integer value
     * @param max  max integer value
     * @return user input
     */
    private int readInt(Text text, int min, int max) {
        return textIO.newIntInputReader().withMinVal(min).withMaxVal(max).read(text.getText());
    }

    /**
     * Read an integer from the {@code textIO}
     *
     * @param text prompt message
     * @return user input
     */
    private int readInt(Text text) {
        return textIO.newIntInputReader().read(text.getText());
    }

    /**
     * Print a text to the {@code textTerminal}
     *
     * @param text      message to print
     * @param arguments arguments for the message
     */
    private void print(Text text, Object... arguments) {
        textTerminal.println(String.format(text.getText(), arguments));
    }

    /**
     * Prompt the user to enter an enum value
     *
     * @param enumClass the enum whose values can be entered
     * @param text      prompt message
     * @param <T>       Enum class
     * @return entered enum
     */
    private <T extends Enum<T>> T readEnumValue(Class<T> enumClass, Text text) {
        return textIO.newEnumInputReader(enumClass).read(text.getText());
    }
}
