package ch.zhaw.catan.io;

/**
 * All text values which can be printed
 *
 * @author Pascal, Ridvan, Nils
 * @version 01.12.2020
 */
public enum Text {
    PROMPT_PLAYERS_COUNT("Please enter the amount of players:"),
    PROMPT_POINT_X_AXIS("Location (X):"),
    PROMPT_POINT_Y_AXIS("Location (Y):"),
    PROMPT_COMMAND("Please enter a command:"),
    PROMPT_TRADE_REQUESTED("Which resource do you request?"),
    PROMPT_TRADE_OFFER("Which resource are you offering?"),
    PROMPT_SETTLEMENT_LOCATION("Please enter the location for your settlement:"),
    PROMPT_CITY_LOCATION("Please enter the location for your city:"),
    PROMPT_ROAD_START_POINT("Please enter the start point for your road:"),
    PROMPT_ROAD_END_POINT("Please enter the end point for your road:"),
    INITIAL_SETTLEMENT("It's the %s Factions turn to place a settlement!"),
    INITIAL_ROAD("It's the %s Factions turn to place a road!"),
    INVALID_INPUT("Invalid input. Please try again."),
    SHOW_RESOURCES("Resources of Faction %s:"),
    SHOW_RESOURCES_VALUE("%-10s: %2d"),
    START_OF_TURN("It's the %1$s Factions turn - Faction %1$s rolled a %2$d!"),
    FACTION_WINNER("Faction %s is the winner!");

    private final String text;

    Text(String text) {
        this.text = text;
    }

    /**
     * Get the text value
     *
     * @return text value
     */
    public String getText() {
        return text;
    }
}
